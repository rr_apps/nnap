require_relative 'lib/generate_advert_helper'
require_relative 'lib/helper'
require_relative 'lib/config_helper'
require_relative 'lib/path_helper'
require_relative 'lib/genrerate_advert'


# Audio Processor
class AudioProcessor < GenrerateAdvert


  def initialize(options = {})
    @logger     = logger_init
    @nil        = temp_path('nil.wav')
    @nil_curr   = temp_path('curr_type.wav')
    @nil_unit   = temp_path('unit_size.wav')
    @nil_music  = temp_path('music.wav')
    @options    = options
    @pb_parts   = 0
    init_required_params(options)
    init_optional_params(options)
  end

  def init_required_params(options)
    @pb         = check_value(options['pb'], 'pb')
    @sp         = check_value(options['sp'], 'sp')
    @pad1       = check_value(options['pad1'], 'pad1')
    @pad2       = check_value(options['pad2'], 'pad2')
    @pad3       = check_value(options['pad3'], 'pad3')
    @val_a1     = check_value(options['val_a1'], 'val_a1')
    @val_b1     = check_value(options['val_b1'], 'val_b1')
  end

  def init_optional_params(options)
    @val_a2     = set_value(options['val_a2'], @val_a1, 'val_a2')
    @val_b2     = set_value(options['val_b2'], @val_b1, 'val_b2')
    @curr_type  = set_value(options['curr_type'], @nil, 'curr_type')
    @unit_size  = set_value(options['unit_size'], @nil, 'unit_size')
    @curr_pad   = set_value(options['curr_pad'], 0.00, 'curr_pad')
    @unit_pad   = set_value(options['curr_pad'], 0.00, 'curr_pad')
    @bitrate    = set_value(options['bitrate'], 256, 'bitrate')
    @music      = set_value(options['music'], @nil_music, 'music')
    @music_pad  = set_value(options['music_pad'], 2, 'music_pad')
    @mastering  = set_value(options['mastering'], '', 'mastering')
  end

  def create_advert
    @logger.info("#{__method__}: Begin! :  #{@options}")
    start_time = Time.now
    pre_prep
    process_advert
    @logger.info("#{__method__}: #{File.basename @sp} Success! : Duration: #{Time.now - start_time}")
  end

  def pre_prep
    create_temp_dir
    split_pb
    prep_files
    prep_pads
  end

  def process_advert
    concat_advert
    prep_music
    mix_and_master
    mp3_convertion
  end
end

if __FILE__ == $PROGRAM_NAME
  @logger = logger_init

  begin
    file = File.read(ARGV[0])
  rescue => err
    @logger.fatal(err)
    puts err
    exit(1)
  end

  begin
    json = JSON.parse(file)
  rescue => err
    @logger.fatal(err)
    puts err
    exit(1)
  end
  AudioProcessor.new(json).create_advert
end
