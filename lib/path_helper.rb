
module PathHelper
  module_function

  def temp_path(file_name)
    File.join(TEMP_DIR, file_name)
  end

  def absolute_paths_of_dir_contents(dir, list = '*')
    path = File.join(dir, list)
    Dir.glob(path) - %w(. .. .DS_Store)
  end

  def create_temp_dir
    FileUtils.rm_rf(TEMP_DIR) while File.exist?(TEMP_DIR)
    Dir.mkdir(TEMP_DIR)
  end

  def which_cli(cli)
    res = `which #{cli}`.chomp
    if res.empty?
      raise("The following dependancy is missing: #{cli}")
      exit(1)
    end
    return res
  end
end

include PathHelper
