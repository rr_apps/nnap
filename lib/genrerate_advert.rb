require_relative 'generate_advert_helper'
require_relative 'helper'
require_relative 'config_helper'
require_relative 'path_helper'

# Generate Advert
class GenrerateAdvert < GenerateAdvertHelper
  include Helper

  def exec_and_log(cmd, descr)
    Dir.chdir(TEMP_DIR)
    res =  system("#{cmd.to_s}")
    if res.eql?(true)
      @logger.info("#{descr}: Success! : #{cmd}")
    else
      @logger.fatal("#{descr}: Fail!: #{cmd}")
      raise "#{descr}: Fail!: #{cmd}"
    end
  end

  def mp3_convertion
    FileUtils.mkpath(File.dirname(@sp)) unless File.exist?(File.dirname(@sp))
    cmd = "#{LAME} #{MP3_PARAMS} #{MP3_BITRATE} -h #{MIXED_ADVERT} #{@sp}"
    exec_and_log(cmd, __method__)
    FileUtils.rm_rf(TEMP_DIR)
  end

  def mix_and_master
    mix_fade = is_music? ? MIX_FADE : ''
    cmd = "#{SOX} -m #{CONCAT_ADVERT} #{TRIMMED_MUSIC} #{MIXED_ADVERT} #{@mastering} #{mix_fade}"
    exec_and_log(cmd, __method__)
  end

  def prep_music
    duration = (`#{SOXI} -D #{CONCAT_ADVERT}`.to_f + 4)
    cmd = "#{SOX} #{STD_VOL} #{@music} #{TRIMMED_MUSIC} #{TRIM} #{duration} #{GAIN}"
    exec_and_log(cmd, __method__)
  end

  def concat_advert
    cmd = "#{SOX} #{STD_VOL} #{build_parts} #{CHANNELS} #{CONCAT_ADVERT} #{CONCAT_PADS}"
    exec_and_log(cmd, __method__)
  end

  def build_parts
    create_pad('pre_pad.wav', is_music_pad)
    concat_parts = ''
    val_num = 0
    @pb_parts.times do |pb_num|
      pb_num += 1
      val_num += 1
      part_builder(concat_parts, pb_num, val_num)
      val_num = 0 if val_num == 2
    end
    'pre_pad.wav' + ' ' + concat_parts
  end

  def is_music?
    @music != temp_path('music.wav')
  end

  def is_music_pad
    return 2 if is_music?
    0.00
  end

  def part_builder(concat_parts, pb_num, val_num)
    concat_parts << part_from_conf(pb_num, val_num)
  end

  def part_from_conf(pb_num, val_num)
    CONF['part'].join(' ').gsub('##', pb_num.to_s).gsub('$$', val_num.to_s) + ' '
  end

  def prep_pads
    PADS_FILES.each do |pad|
      pad_file = pad.delete('@') + TEMP_FORMAT
      pad_len = eval(pad)
      create_pad(pad_file, pad_len)
    end
  end

  def prep_files
    check_create
    create_silent_music if @music == temp_path('music.wav')
    PREP_FILES.each do |file|
      path = eval(file)
      file_name = file.delete('@') + TEMP_FORMAT
      trim_ends(path, file_name)
    end
  end

  def check_create
    %w(nil unit_size curr_type).each do |file|
      create_pad(file  + TEMP_FORMAT) unless File.exist?(file)
    end
  end

  def split_pb
    cmd = "#{SOX} #{@pb} #{SPLIT_PART} #{SPLIT_PB}"
    exec_and_log(cmd, __method__)
    parts = Dir.entries(TEMP_DIR) - %w(. .. .DStore)
    pb_parts_trim(parts)
  end
end
