
# Generate Advert Helper
class GenerateAdvertHelper
  def trim_ends(in_file, out_file)
    out_file = temp_path(out_file)
    cmd = %(#{SOX} #{in_file} #{out_file} #{TRIM_ENDS})
    exec_and_log(cmd, __method__)
  end

  def create_pad(pad_file, gap = 0.0001)
    pad_file = temp_path(pad_file)
    cmd = "#{SOX} #{pad_params(gap, pad_file)}"
    exec_and_log(cmd, __method__)
  end

  def create_silent_music
    cmd = "#{SOX} #{silent_music_params}"
    exec_and_log(cmd, __method__)
  end

  def silent_music_params
    CREATE_PAD.gsub('$$pad_file', 'music.wav')
        .gsub('$$gap', '120')
  end

  def pad_params(gap, pad_file)
    CREATE_PAD.gsub('$$pad_file', pad_file)
              .gsub('$$gap', gap.to_s)
  end

  def pb_parts_trim(parts)
    count = 0
    parts.each do |part|
      if File.size(part) > 100
        pb = "pb_#{count += 1}#{TEMP_FORMAT}"
        trim_ends(temp_path(part), pb)
      end
    end
    @pb_parts = count
  end
end
