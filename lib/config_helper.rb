require 'json'
require 'fileutils'
require 'logger'
require 'pathname'
require 'yaml'

require_relative 'path_helper'

module ConfigHelper
  module_function

  path          = Pathname(__FILE__).dirname
  config_file   = File.join(path, 'config.yml')

  # Congiguration file containing all the settings for the app
  CONF          = YAML.load(File.read(config_file))
  # SoX tool path
  SOX           = which_cli('sox')
  # SoXi tools path
  SOXI          = which_cli('soxi')
  # Lame tool path
  LAME          = which_cli('lame')
  # The bitrate at which MP3's will be encoded
  MP3_BITRATE   = CONF['mp3_bitrate']
  # The params required for the MP# encoder
  MP3_PARAMS    = CONF['mp3_params']
  # The temporary format used for audio processing
  TEMP_FORMAT   = CONF['temp_format']
  # List of file to be prepped
  PREP_FILES    = CONF['prep_files'].split(' ')
  # List of pad files
  PADS_FILES    = CONF['pad_files'].split(' ')
  # The temp directory where the audio files being processed will be located.
  TEMP_DIR	    = ENV['NNAP_TEMP_DIR'] || File.join(Dir.pwd, 'temp')

  SPLIT_PART    = File.join(TEMP_DIR, CONF['split_pb_part'] + TEMP_FORMAT)
  MIXED_ADVERT  = File.join(TEMP_DIR, CONF['mixed_advert']  + TEMP_FORMAT)
  TRIMMED_MUSIC = File.join(TEMP_DIR, CONF['trimmed_music'] + TEMP_FORMAT)
  CONCAT_ADVERT = File.join(TEMP_DIR, CONF['concat_advert'] + TEMP_FORMAT)

  TRIM_ENDS     = CONF['trim_ends'].chomp
  CREATE_PAD    = CONF['create_pad'].chomp
  SPLIT_PB      = CONF['split_pb'].join(' ')
  MIX_FADE      = CONF['mix_fade'].chomp

  CHANNELS      = CONF['channels']
  CONCAT_PADS   = CONF['concat_pads']

  STD_VOL       = CONF['standard_volume']
  TRIM          = CONF['trim']
  GAIN          = CONF['gain']

  MASTERING     = CONF['mastering']
end

include ConfigHelper
