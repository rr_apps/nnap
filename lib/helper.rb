# Helper
module Helper
  module_function

  def load_envs
    ENV['NNAP_TEMP_DIR']
  end

  def check_value(opt, param_name)
    res = param?(opt)
    @logger.error("Fail! : Value for '#{param_name}' not found. Exiting! ") unless res
    exit(1) unless res
    opt
  end

  def set_value(opt_true, opt_false, param_name = '')
    res = param?(opt_true)
    @logger.warn("Value for '#{param_name}' not found. Using alternate value #{opt_false}") unless res
    res ? opt_true : opt_false
  end

  def param?(param)
    return false if param.nil?
    ['nil', '', ' '].each { |i| return false if i == param }
    true
  end

  def report_missing_param(msg)
    @logger.warn("#{msg} : is missing a param")
  end

  def logger_init
    nnap_log_file = ENV['NNAP_LOG_FILE'] ||  File.join(Dir.pwd, 'nnap.log')
    if nnap_log_file
      FileUtils.mkpath(File.dirname(nnap_log_file)) unless File.exists?(File.dirname(nnap_log_file))
      Logger.new(nnap_log_file)
    else
      puts 'Please set a log file path in the following environmental variable: NNAP_LOG_FILE'
      exit(1)
    end
  end
end

include Helper
