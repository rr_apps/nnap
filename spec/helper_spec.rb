require 'helper'
require 'logger'

describe Helper do
  include Helper

  # obj = Helper.new
  before(:each) do
    logger = Logger.new(STDOUT)

    describe '.check_value' do
      it 'will return a value' do
        expect(check_value('0.9', 'test param')).to eq('0.9')
      end

      it 'will log an error & exit' do
        # expect(obj.check_value('0.9', 'test param')).to eq('0.9')
        # expect(logger).to receive(:error).with("your error message")
      end
    end

    describe '.param?' do
      it 'will return false' do
        expect(param?(nil)).to be(false)
        expect(param?('nil')).to be(false)
        expect(param?('')).to be(false)
        expect(param?(' ')).to be(false)
      end

      it 'will return true' do
        expect(param?(0.5)).to be(true)
        expect(param?('../path')).to be(true)
      end
    end
  end
end
