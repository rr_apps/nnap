# Natural Numbers - Audio Processor #

This is the concatenation & audio processing code for Natural Numbers.
It is used as a CLI in the following apps:

* [PB Re-RecordTester ver 3.0](https://bitbucket.org/rr_apps/pb-re-recordtester-ver-3.0)
* [MeatMarket ver 2.0](https://bitbucket.org/rr_apps/meat_market_v2.0/overview)
* RR Control Center


### Requirements ###

* Ruby ver 2.0.0 +
* SoX - [Sound eXchange audio processor](http://sox.sourceforge.net/)
* LAME - [MPEG Audio Layer III (MP3) encoder](http://lame.sourceforge.net/)


### Setup ####

Add temp dir and log file environments

	echo "export NNAP_LOG_FILE='/Users/davidteren/Projects/RR/factory_nnap/logs/nnap.log'" >> ~/.bash_profile
	echo "export NNAP_TEMP_DIR='/Users/davidteren/Projects/RR/factory_nnap/temp'" >> ~/.bash_profile
	source ~/.bash_profile

### Usage ###

Run the app by calling `nn_audio_processor.rb` and passing it the `JSON` file it should process.
The `JSON` file should contain the parameters required to generate the advert.

    ruby nn_audio_processor.rb SPM01012010.json

 Typically a JSON file includes the following params:

 - Description | Value Type | Required
 - PB (Pre-Recorded body) file | absolute path | Required
 - PB (Pre-Recorded body) file | absolute path | Required
 - Final SP filename | absolute path | Required
 - First value file | absolute path | Required
 - Second value file | absolute path | Required
 - First value round robin file | absolute path | Optional
 - Second value round robin file | absolute path | Optional
 - Currency type file | absolute path | Optional
 - Unit size file | absolute path | Optional
 - Music file | absolute path | Optional
 - Pad1 value | string or value  | Optional
 - Pad2 value | string or value  | Optional
 - Pad3 value | string or value  | Optional
 - Currency Pad | string or value  | Optional
 - Mastering params | string | Optional
 - MP3 encoding | string or value  | Optional

Typically a JSON file will look like this.

	{
	  "pb": "~/assets/pb/270L02_mm000120.mp3",
	  "sp": "~/assets/final/SPM0100001.mp3",
	  "voice_code": "270",
	  "val_a1": "~/assets/val/270A1L02089.mp3",
	  "val_b1": "~/assets/val/270B1L02099.mp3",
	  "val_b2": "~/assets/val/270B2L02099.mp3",
	  "unit_size": "~/assets/val/270USL021kg.mp3",
	  "music": "~/assets/music/002.mp3",
	  "pad1": "0.12",
	  "pad2": "0.09",
	  "pad3": "0.5",
	  "mastering": "gain -10 compand 0,0.2 -90,-90,-70,-70,-60,-30,-2,-8,-2,-7 -2 equalizer 40 .71q +4 equalizer 80 1.10q +0 equalizer 240 1.80q -3 equalizer 500 .71q +0 equalizer 1000 2.90q +0 equalizer 2500 .51q +1.5 equalizer 8500 .71q -1.5 equalizer 17000 .71q +4 sinc 90-14000 gain -n -9.1"
	}


### Testing ###


	git clone git@bitbucket.org:rr_apps/nn_audio_processor.git
	cd nn_audio_processor
	git fetch && git checkout qa
	cp -a examples/assets ~/assets
	ruby audio_processor.rb examples/SPM0100001.json

You should see a newly created SPM file in `~/assets/final`

For an example that uses a currency drop-in

	ruby audio_processor.rb examples/SPM0100002_currency.json
